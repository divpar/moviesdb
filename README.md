# MoviesDB

MoviesDB is an api interface to provide all the data of Movies.
    
### API Calls


#### GET /api/movies

Paramters:
```
name | String in the name of the movie
director | Name of the Director
min_popularity | Popularity (Decimial)
max_popularity | Popularity (Decimial)
genre | Genre name
min_score | imdb_score (Decimal)
max_score | imdb_score (Decimal)
```

All the above parameters are optional

#### POST /api/movies

Headers:
Require Authentication for the API
```
Authentication: Token <auth_token> 
```
Parameters
```
name | Name of the Movie
director | Name of the Director
genre | list of genre
popularity | Decimal input
imdb_score | Decimal input
```

#### POST /login

Paramaters
```
username | login username
password | login password
```


### Installation

MoviesDB requires Python 2.7 to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd MoviesDB
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver --settings=MovieDB.setting.local
```

For production environments...

```sh
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver --settings=MovieDB.setting.production
```

