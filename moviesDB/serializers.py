from models import Movie, Director, GENRE_LIST
from rest_framework import serializers


class DirectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Director
        fields = ["name"]


class MovieSerializer(serializers.ModelSerializer):
    director = serializers.SlugRelatedField(
        queryset=Director.objects.all(),
        slug_field='name'
     )
    genre = serializers.MultipleChoiceField(GENRE_LIST)

    class Meta:
        model = Movie
        fields = ('name', 'popularity', 'director', 'genre', 'imdb_score')



