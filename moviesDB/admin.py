# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import Movie, Director

from django.contrib import admin

# Register your models here.

admin.site.register(Movie)
admin.site.register(Director)