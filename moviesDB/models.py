# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from multiselectfield import MultiSelectField


GENRE_LIST = [
  "Adventure",
  " Family",
  " Fantasy",
  " Musical",
  "Action",
  " Adventure",
  " Sci-Fi",
  " Drama",
  " War",
  "Horror",
  " Mystery",
  " Thriller",
  " Horror",
  "Drama",
  " Romance",
  "Animation",
  "Crime",
  " History",
  " Western",
  " Film-Noir",
  "Thriller",
  "Fantasy",
  "Musical",
  " Comedy",
  " Documentary",
  " Music",
  "Comedy",
  "Short",
  " Action",
  "Family",
  " Sport",
  " Crime",
  "Mystery",
  " Short",
  "Talk-Show",
  " Biography",
  "Biography",
  "Romance",
  "Game-Show",
  "Western",
  "Documentary",
  " News",
  " Talk-Show",
  "Adult",
  "Film-Noir",
  " Reality-TV"
]

GENRES = (map(lambda x: (x, x), GENRE_LIST))


class Director(models.Model):
    name = models.CharField(max_length=500, unique=True)

    def __unicode__(self):
        return self.name


class Movie(models.Model):
    name = models.CharField(max_length=1000)
    popularity = models.DecimalField(decimal_places=2, max_digits=4)
    director = models.ForeignKey(Director)
    genre = MultiSelectField(choices=GENRES)
    imdb_score = models.DecimalField(decimal_places=1,max_digits=2)

    def __unicode__(self):
        return self.name





