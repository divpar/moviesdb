# -*- coding: utf-8 -*-
import json

from rest_framework.permissions import IsAdminUser, BasePermission, AllowAny
from models import Director, Movie, GENRES
from django_filters import rest_framework
from rest_framework import viewsets
from serializers import MovieSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.status import HTTP_200_OK
from rest_framework.response import Response



class ReadOnly(BasePermission):
    """
    Defining permission for getting the Movies
    """
    def has_permission(self, request, view):
        return request.method == 'GET'


class MovieFilter(rest_framework.FilterSet):
    """
    Designed to implement search in the API
    """
    min_score = rest_framework.NumberFilter(field_name="imdb_score", lookup_expr='gte')
    max_score = rest_framework.NumberFilter(field_name="imdb_score", lookup_expr='lte')
    min_popularity = rest_framework.NumberFilter(field_name="popularity", lookup_expr='gte')
    max_popularity = rest_framework.NumberFilter(field_name="popularity", lookup_expr='lte')
    genre = rest_framework.ChoiceFilter(field_name='genre', lookup_expr='contains', choices=GENRES)
    name = rest_framework.CharFilter(field_name='name', lookup_expr='contains')

    class Meta:
        model = Movie
        fields = ['name', 'director', 'min_popularity', 'max_popularity', 'genre', 'min_score', 'max_score']


class MoviesViewSet(viewsets.ModelViewSet):
    """
    Get the list of movies and also apply filter on it.

    Also add a new Movie if admin User
    """
    permission_classes = [(IsAdminUser | ReadOnly)]
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()
    filter_backends = (rest_framework.DjangoFilterBackend,)
    filter_class = MovieFilter


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def migrate(request):
    f = open("data.json", 'r')
    data = f.read()
    json_data = json.loads(data)
    for entry in json_data:
        entry['director'] = Director.objects.get_or_create(name = entry['director'])[0]
        d = Movie()

        for k, v in entry.iteritems():
            setattr(d, k.lower(), v)

        d.save()
    return Response({}, status=HTTP_200_OK)

